package Buscador;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ZapatosSeguridad {
private WebDriver driver;
	
	
	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver","Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://www.falabella.com/falabella-cl/");
	}
	@Test 
	public void BuscadorZapatosSeguridad() { 
		//driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		WebDriverWait ewait = new WebDriverWait(driver, 5);
		WebElement close = ewait.until(ExpectedConditions.elementToBeClickable(By.className("dy-lb-close")));
		close.click();
		//driver.findElement(By.className("dy-lb-close")).click();
		driver.findElement(By.xpath("/html/body/div[1]/nav/div[3]/div/div[2]/div/div[1]/p")).click();
		
		
		Actions a = new Actions(driver);
		WebElement Zapatos = driver.findElement(By.xpath("/html/body/div[1]/nav/div[3]/div/div[2]/div/div[1]/div/div/div[2]/div/div[1]/ul/li[13]/span"));
		a.moveToElement(Zapatos).perform();
		
		driver.findElement(By.linkText("Zapatos de seguridad")).click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/section[1]/div[4]/div/ul/li[1]/div/div/input")).sendKeys("BOTAS");
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[3]/section[1]/div[4]/div/ul/li[1]/div/ul/li/label/input")).click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.id("testId-pod-12575201")).click();
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		driver.findElement(By.xpath("/html/body/div[1]/div/section/div[1]/div[1]/div[2]/section[2]/div[2]/div/div[1]/div[2]/div[2]/div[2]")).click();
		driver.findElement(By.xpath("/html/body/div[1]/div/section/div[1]/div[1]/div[2]/section[2]/div[2]/div/div[2]/div[2]/div")).click();
		
		for (int i = 1; i < 4; i++){
		
			WebElement incrementar = ewait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div/div/div/div/div/div/div[2]/div/div/div/div/div[3]/div/button[2]")));
			incrementar.click();
		}
		
			driver.findElement(By.id("linkButton")).click();
			System.out.println(driver.getCurrentUrl());
			Assert.assertEquals(("https://www.falabella.com/falabella-cl/basket"), driver.getCurrentUrl());
		
	}
	
	@After 
    public void tearDown() {
    	driver.close();
    }
}
